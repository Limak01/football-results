const competitions = [
    {
        name: 'La Liga',
        img: 'spain.png',
        id: 2014,
    },
    {
        name: 'Bundesliga',
        img: 'germany.png',
        id: 2002,
    },
    {
        name: 'Premere League',
        img: 'england.png',
        id: 2021,
    },
    {
        name: 'Ligue 1',
        img: 'france.png',
        id: 2015,
    },
    {
        name: 'Seria A',
        img: 'italy.png',
        id: 2019,
    },
]

export default competitions