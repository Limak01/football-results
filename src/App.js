import Navbar from "./components/Navbar/Navbar"
import Results from "./components/Results/Results"

function App() {
  return (
    <div className="App">
      <Navbar />
      <Results />
    </div>
  );
}

export default App;
