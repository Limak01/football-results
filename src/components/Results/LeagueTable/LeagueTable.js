import { useState, useEffect } from 'react'
import req from '../../../requests'
import './LeagueTable.css'
import axios from 'axios'
import competitions from '../../../competitions'

const LeagueTable = () => {
    const [leagueTable, setLeagueTable] = useState([])
    const [competitionIndex, setCompetitionIndex] = useState(0)

    const getLeagueTable = async () => {
        const res = await axios.get(`${req.baseUrl}/v2/competitions/${competitions[competitionIndex].id}/standings`, {headers: {'X-Auth-Token' : `${req.API_KEY}`}})
        setLeagueTable(res.data.standings[0].table)
        console.log(res.data)
    }

    const nextLeague = () => {
        setCompetitionIndex(prevIndex => {
            if(prevIndex + 1 >= competitions.length) {
                return 0
            }
            return prevIndex + 1
        })
    }
    const prevLeague = () => {
        setCompetitionIndex(prevIndex => {
            if(prevIndex - 1 < 0) {
                return competitions.length - 1
            }
            return prevIndex - 1
        })
    }

    useEffect(() => {
        getLeagueTable()
    }, [competitionIndex])

    return (
        <div className="league-table">
            <div className="table-nav">
                <div className="left-btn table-btn" onClick={prevLeague}>
                    <i className="fas fa-angle-left"></i>
                </div>
                <span className="table-title">{competitions[competitionIndex].name}</span>
                <div className="right-btn table-btn" onClick={nextLeague}>
                    <i className="fas fa-angle-right"></i>
                </div>
            </div>
            <div className="table-container">
                <table className="league-tbl">
                    <thead>
                        <tr className="thead-items">
                            <td colSpan="2">
                                <span>Club</span>
                            </td>
                            <td className="matches-played">
                                <span>MP</span>
                            </td>
                            <td className="wins">
                                <span>W</span>
                            </td>
                            <td className="draws">
                                <span>D</span>
                            </td>
                            <td className="loses">
                                <span>L</span>
                            </td>
                            <td className="goal-difference">
                                <span>GD</span>
                            </td>
                            <td className="points">
                                <span>Pkt.</span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            leagueTable.map((item, index) => {
                                return (
                                    <tr className="tbody-items">
                                        <td className="place">
                                            <span>{index + 1}</span>
                                        </td>
                                        <td className="team" title={item.team.name}>
                                            {/* <span>{item.team.name}</span> */}
                                            <img src={`${item.team.crestUrl}`} alt="club logo" width="25px" height="25px"/>
                                        </td>
                                        <td className="matches-played">
                                            <span>{item.playedGames}</span>
                                        </td>
                                        <td className="wins">
                                            <span>{item.won}</span>
                                        </td>
                                        <td className="draws">
                                            <span>{item.draw}</span>
                                        </td>
                                        <td className="loses">
                                            <span>{item.lost}</span>
                                        </td>
                                        <td className="goal-difference">
                                            <span>{item.goalDifference}</span>
                                        </td>
                                        <td className="points">
                                            <span>{item.points}</span>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default LeagueTable