import './Competitions.css'
import competitions from '../../../competitions'


const Competitions = () => {
    return (
        <div className="competitions">
            <div className="competitions-title">
                <h3>COMPETITIONS</h3>
            </div>
            <div className="competitions-list">
                {
                    competitions.map((item, index) => {
                        return (
                            <div key={index} className="competition-item">
                                <div className="competition-image">
                                    {
                                        item?.img ? 
                                            <img src={`${process.env.PUBLIC_URL}/images/${item.img}`} width="25px"/>
                                        : null
                                    }
                                </div>
                                <div className="competition-name">
                                    <h4>{item.name}</h4>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Competitions