import Competitions from "./Competitions/Competitions"
import LeagueTable from "./LeagueTable/LeagueTable"
import Matches from "./Matches/Matches"
import './Results.css'

const Results = () => {
    return (
        <div className="container results-container">
            <div className="l-side">
                <div className="l-side-items">
                    <Competitions />
                    <LeagueTable />
                    <div className="copyright">
                        <span>&#169; 2021 Limak</span>
                    </div>
                </div>
            </div>
            <div className="r-side">
                <Matches />
            </div>
        </div>
    )
}

export default Results
