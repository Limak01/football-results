import React, { useState, useEffect } from 'react'
import './SingleCompetition.css'
import SingleMatch from '../SingleMatch'

const SingleCompetition = ({competition_index, competition_id, matches, name, img}) => {
    const [isHidden, setIsHidden] = useState(false)

    const noMatches = () => {
        for(let i = 0; i < matches.length; i++) {
            if(matches[i].competition.id === competition_id) {
                return false
            }
        }
        return true
    }

    
    return (
        <>
            <tr>
                <td className="filter" colSpan="4">
                    <div className="filter-data">
                        <div className="filter-league">
                            <div className="filter-img">
                                <img src={`${process.env.PUBLIC_URL}/images/${img}`} alt="france flag" width="25px"/>
                            </div>
                            <div className="filter-title">
                                <span>{name}</span>
                            </div>
                        </div>
                        <div className="competition-buttons">
                            <div className="hide-competition-button" onClick={() => setIsHidden(!isHidden)}>
                                <i className={`fas fa-angle-${isHidden ? 'down' : 'up'}`}></i>
                            </div>
                            {/* <div className="remove-competition">
                                <i className="fas fa-times"></i>
                            </div> */}
                        </div>
                    </div>
                </td>
            </tr>
                {
                   
                   noMatches() ?
                        !isHidden && 
                            <tr className="no-matches-err">
                                <td colSpan="4" className="no-matches">
                                    <span>
                                        No matches found for <span className="diff-font">{name}</span> at this day.
                                    </span>
                                </td>
                            </tr>
                    : null
                }
                {
                   !isHidden ? matches.map((item, index) => {
                        return item.competition.id ===  competition_id ? <SingleMatch match={item} key={index}/> : null
                    })
                    : null 
                } 
        </>  
    )
}

export default SingleCompetition