import { useState, useEffect } from 'react'
import './Matches.css'
import axios from 'axios'
import req from '../../../requests'
import competitions from '../../../competitions'
import SingleCompetition from './SingleCompetition/SingleCompetition'


const Matches = () => {
    const [matches, setMatches] = useState([])

    const getMatches = async (date) => {
        const res = await axios.get(`${req.baseUrl}/v2/matches?dateFrom=${date}&dateTo=${date}`, {headers: {'X-Auth-Token' : `${req.API_KEY}`}})
        setMatches(res.data.matches)
    }

    const getDate = (dayAhead) => {
        const date = new Date()
        if(dayAhead) {
            date.setDate(date.getDate() + dayAhead)
        }
        const year = date.getFullYear()
        let month = date.getMonth() + 1
        let day = date.getDate()
        if(month < 10) month = '0' + month.toString()
        if(day < 10) day = '0' + day.toString()
        return `${year}-${month}-${day}`
    }

    const [date, setDate] = useState(getDate(0))
    const [showDaysAhead, setShowDaysAhead] = useState(0)

    const handleDateIncrement = () => {
        setShowDaysAhead(prevState => prevState + 1)
    }

    const handleDateDecrement = () => {
        setShowDaysAhead(prevState => prevState - 1)
    }

    useEffect(() => {
        getMatches(date)
        const refreshResults = setInterval(() => {
            getMatches(date)
        }, 60000)

        return () => {
            clearInterval(refreshResults)
        }
    }, [date])

    useEffect(() => {
        setDate(getDate(showDaysAhead))
    }, [showDaysAhead])

    return (
        <div className="matches">
            <div className="matches-day">
                <div className="go-back-button go-btn" onClick={handleDateDecrement}>
                    <i className="fas fa-angle-left"></i>
                </div>
                <h2>{date}</h2>
                <div className="go-forward-button go-btn" onClick={handleDateIncrement}>
                    <i className="fas fa-angle-right"></i>
                </div>
            </div>
            <div className="matches-list">
                <table>
                    <tbody>
                        {
                            competitions.map((competition, index) => {
                                return  <SingleCompetition key={index} competition_index={index} competition_id={competition.id} matches={matches} img={competition.img} name={competition.name} />
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Matches