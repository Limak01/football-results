import './SingleMatch.css'

const SingleMatch = ({match}) => {
    
    const getMatchTime = () => {
        const dateTime = match.utcDate.split('T')
        const tempTime = dateTime[1].split(':')
        return `${parseInt(tempTime[0]) + 2}:${tempTime[1]}`
    }

    return (
        <tr className="single-match">
            <td className="match-status">
                {
                    match?.status === 'IN_PLAY' ? 
                        <span className="match-is-live status">LIVE</span>
                    : 
                        match?.status === 'PAUSED' ?
                        <span className="match-is-live status">HT</span>
                        : 
                            match?.status === 'FINISHED' ?
                                <span className="match-st status">FT</span>
                            :
                                <span className="match-st status">{getMatchTime()}</span>
                }
            </td>
            <td className="team home-team">
                <span>{match.homeTeam.name}</span>
            </td>
            <td className="result">
                {
                    match.score.fullTime.homeTeam !== null  ?
                    <span>
                        {match.score.fullTime.homeTeam ? match.score.fullTime.homeTeam : 0}
                         : 
                        {match.score.fullTime.awayTeam ? match.score.fullTime.awayTeam : 0}
                    </span>
                    :
                    <span>
                        -
                    </span>
                }
            </td>
            <td className="team away-team">
                <span>{match.awayTeam.name}</span>
            </td>
        </tr>
    )
}

export default SingleMatch