import './Navbar.css'

const Navbar = () => {
    return (
        <header>
            <div className="navbar">
                <div className="container nav-container">
                    <div className="logo">
                        <span className="logo-span first-line">Football</span>
                        <span className="logo-span second-line">Results</span>
                    </div>
                    <div className="nav-links">
                        <div className="single-link">
                            <i className="far fa-futbol nav-link-icon"></i>
                            <span className="link-title">HOME</span>
                        </div>
                        <div className="single-link middle-link">
                            <i className="fas fa-ticket-alt nav-link-icon"></i>
                            <span className="link-title">BET</span>
                        </div>
                        <div className="single-link">
                            <i className="fas fa-sign-in-alt nav-link-icon"></i>
                            <span className="link-title">LOGIN</span>
                        </div>
                    </div>
                </div>
            </div>    
        </header> 
    )
}

export default Navbar